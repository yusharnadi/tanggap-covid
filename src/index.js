import regeneratorRuntime from "regenerator-runtime";
import "./style.css";
import main from "./scripts/main";

document.addEventListener("DomContentLoaded", main());
