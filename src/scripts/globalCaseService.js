import axios from "axios";
const globalCaseService = async () => {
	const data = {
		count: 0,
		positif: 0,
		sembuh: 0,
		meninggal: 0,
	};
	const globalUri = "https://statistik.singkawangkota.go.id/nasional/global";
	try {
		const request = await axios.get(globalUri);
		const data = request.data;
		return data;
	} catch (data) {
		return data;
	}
};

export default globalCaseService;
