import "../components/GlobalStat";
import "../components/IndoStat";
import "../components/TabelProv";
import globalCaseService from "./globalCaseService";
import IndoCaseService from "./IndoCaseService";
import ProvCaseService from "./ProvCaseService";
const main = async () => {
	const globalStatElement = document.querySelector("global-stat");
	const indoStatElement = document.querySelector("indo-stat");
	const tableProv = document.querySelector("table-prov");

	globalStatElement.data = await globalCaseService();
	indoStatElement.data = await IndoCaseService();
	tableProv.data = await ProvCaseService();
};

export default main;
