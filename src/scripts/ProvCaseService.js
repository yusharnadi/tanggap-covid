import axios from "axios";
const ProvCaseService = async (status) => {
	const Uri = "https://statistik.singkawangkota.go.id/nasional/provinsi";
	try {
		const request = await axios.get(Uri);
		const data = request.data;
		return data.data;
	} catch (error) {
		return error;
	}
};

export default ProvCaseService;
