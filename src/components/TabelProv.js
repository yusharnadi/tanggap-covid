class TabelProv extends HTMLElement {
	constructor() {
		super();
		this._data = [];
		this.render();
	}

	set data(data) {
		this._data = data;
		this.querySelector("tbody").innerHTML = "";
		this._data.map((col, index) => {
			this.querySelector("tbody").innerHTML += `
            <tr>
                <td class="border px-4 py-2">${index + 1}</td>
                <td class="border px-4 py-2">${col.attributes.Provinsi}</td>
                <td class="border px-4 py-2">${col.attributes.Kasus_Posi}</td>
                <td class="border px-4 py-2">${col.attributes.Kasus_Semb}</td>
                <td class="border px-4 py-2">${col.attributes.Kasus_Meni}</td>
            </tr>
            `;
		});
	}

	render() {
		this.className = "container mx-auto mb-10 w-5/6 lg:w-full";
		this.innerHTML = `
        <div class="bg-white rounded-lg shadow-lg ">
            <div class="border-b-2 p-4">
                <h5 class="text-gray-700 font-bold text-center">DATA KASUS COVID19 di INDONESIA BERDASARKAN PROVINSI
                </h5>
            </div>
            <div class="p-4 w-full h-120 overflow-auto">
                <table class="table-fixed text-gray-600 text-xs lg:text-base">
                    <thead>
                        <tr>
                            <th class="border w-1/12 px-4 py-2">NO.</th>
                            <th class="border w-3/6 px-4 py-2">PROVINSI</th>
                            <th class="border w-1/6 py-2">POSITIF</th>
                            <th class="border w-1/6 px-4 py-2">SEMBUH</th>
                            <th class="border w-1/6 px-4 py-2">MENINGGAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="5" class="border w-1/12 px-4 py-2">Loading data . . .</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        `;
	}
}

customElements.define("table-prov", TabelProv);
