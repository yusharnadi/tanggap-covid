class GlobalStat extends HTMLElement {
	constructor() {
		super();
		this._data = {
			count: 0,
			positif: 0,
			sembuh: 0,
			meninggal: 0,
		};
		this.render();
	}

	set data(data) {
		this._data = data;
		this.render();
	}

	render() {
		this.className = "flex flex-col justify-between items-center bg-white shadow-2xl rounded-lg my-16 py-6 px-16 w-4/5 mx-auto";
		this.innerHTML = `
        <h4 class="text-2xl font-bold text-gray-600 text-center">GLOBAL COVID19 CASE</h4>
        <div class="stats flex w-full  lg:flex-row flex-col justify-between items-center my-6">
            <div class="stat-item flex flex-col items-center justify-between ">
                <span class="text-red-400 text-5xl font-bold">${this._data.count}</span>
                <span class="text-gray-600">Negara Terdampak</span>
            </div>
            <div class="v-line bg-gray-400"></div>
            <div class="stat-item flex flex-col items-center justify-between ">
                <span class="text-red-400 text-5xl font-bold">${this._data.positif}</span>
                <span class="text-gray-600">Kasus Terkonfirmasi</span>
            </div>
            <div class="v-line bg-gray-400"></div>
            <div class="stat-item flex flex-col items-center justify-between ">
                <span class="text-green-400 text-5xl font-bold">${this._data.sembuh}</span>
                <span class="text-gray-600">Sembuh</span>
            </div>
            <div class="v-line bg-gray-400"></div>
            <div class="stat-item flex flex-col items-center justify-between ">
                <span class="text-red-400 text-5xl font-bold">${this._data.meninggal}</span>
                <span class="text-gray-600">Meninggal</span>
            </div>
        </div>
        `;
	}
}

customElements.define("global-stat", GlobalStat);
