const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
	mode: "development",
	// Optional for webpack-dev-server
	devServer: {
		watchContentBase: true,
		// contentBase: path.resolve(__dirname, "dist"),
		open: true,
	},
});
